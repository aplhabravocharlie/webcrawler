//
// Serviciu manager de spideri.
//

// Declaram resursele folosite de acest serviciu.
let db;
let mq;

var time;


//Declarare parametrii control
let home = 'https://www.emag.ro/homepage';
let next = home;
let sessionLogs = 0;
//
let title = 'manager-emag-'+Math.floor((Math.random()*10e8-10e7)+10e7);

let expirationTime = 60;      //timpul in minute dupa care un link paote sa fie rescanat
let spiderCoolOff = 5;        //timpul intre apeluri pt spider

// Configuratii si dependente.
// Mentionam proprietatea port pentru portul folosit de serviciu.
// Proprietatea port este necesara doar daca folosesti bibliotecile common http sau rtc.
// Dependencies mentioneaza care dintre bibliotecile common sunt folosite.
module.exports.conf = {
    port: 8080,             // portul pe care porneste serviciul (optional aici)
    dependencies: [
        '../common/db',     // folosim baza de date
        '../common/mq',     // folosim canalul de mesaje
    ]
};

// Functia de initializare a serviciului.
// Daca initializarea a esuat, chemam cbFail cu mesajul de eroare.
// Daca initializarea a reusit, chemam cbSuccess.
// Aceasta functie este teoretic optionala, nu este necesar sa o definesti. Practic
// insa, vom initializa cel putin mq in toate serviciile.
module.exports.init = function (cbSuccess, cbFail) {

    console.log("Service is starting...");

    // Monitorizare
    monit = require('../common/monit').getSocket();

    // Inregistrare.
    monit.emit('register-service', {
        title: title,
        ip: require('../common/monit').getIp(),
        type:"manager",
        minMemoryValue:"0",
        maxMemoryValue:"100", 
        actualMemoryValue:(Math.random()*100-40)+40,
        minConnectionValue:"0", 
        maxConnectionValue:"100", 
        actualConnectionValue: (Math.random()*100-40)+40, 
        state:"active"
    });

    // Preluam resursele pe care le-am mentionat.
    db = require('../common/db').getClient();
    mq = require('../common/mq').getClient();

    // codul vechi de init
    time = new Date();
    let newURL = {
        url: home,
        timeDiscovered: time.getTime()
    };
    db.collection('que1').insert(newURL, function (err, result) {
        if (err) {
            time = new Date();
            console.log('<' + time.toLocaleTimeString() + '>___***___SpiderManager: Could not add URL to DB que.');
        } else {
            time = new Date();
            console.log('<' + time.toLocaleTimeString() + '>___INI___SpiderManager: Added INIT url to que in DB.');
        }
    });


    // Anuntam ca am terminat initializarea cu succes.
    cbSuccess();

};


// Functia de oprire a serviciului.
// Dupa ce am curatat ce era necesar, chemam functia exit.
// Nu avem voie sa aruncam erori aici.
// Nu putem opri incheierea serviciului.
// Aceasta functie este optionala, nu este necesar sa o definesti. Este
// prezenta aici doar ca exemplu.
module.exports.final = function (exit) {

    console.log("Service is stopping...");

    // De-Inregistrare.
    monit.emit('deregister-service', {
        title: title,
        ip: require('../common/monit').getIp(),
        type:"manager",
        minMemoryValue:"0",
        maxMemoryValue:"100", 
        actualMemoryValue:(Math.random()*100-40)+40,
        minConnectionValue:"0", 
        maxConnectionValue:"100", 
        actualConnectionValue: (Math.random()*100-40)+40, 
        state:"active"
    });

    // Nu avem ce curata.
    exit();

};

// Functia de rulare permanenta a serviciului.
// Aici va puteti atasa la canalele de mesaje.
module.exports.run = function () {

    // directive:
    // comunicati cu echipa serviceSpider pentru a afla
    // ce mesaje trebuie preluate si date
    //
    // in codul de mai jos aveti exact ce era inainte
    // (mai putin o redenumire a variabilei service->mq)
    // deci va puteti folosi de el pentru a va orienta cum
    // sa folositi baza de date, etc

    // atentie!
    // cand trimiteti mesaje, este recomandat sa folositi obiecte
    // nu primitive
    // DA: mq.emit('mesaj', { url: "gfdglfgldfmgl" })
    // NU: mq.emit('mesaj', "gfdglfgldfmgl")


    function putToLog(newLog) {
        db.collection('log1').findOne({url: {$eq: newLog.url}}, function (err, result) {
            if (err) {
                time = new Date();
                console.log('<' + time.toLocaleTimeString() + '>___OOO___SpiderManager: Problem reading url');
            } else {
                if (result == null) {
                    time = new Date();
                    console.log('<' + time.toLocaleTimeString() + '>___XXX___SpiderManager: INSERTING into log!');

                    db.collection('log1').insert(newLog, function (err, result) {
                        if (err) {
                            time = new Date();
                            console.log('<' + time.toLocaleTimeString() + '>___OOO___SpiderManager: Could not add URL to DB LOG.');
                        } else {
                            time = new Date();
                            ///console.log('<'+time.toLocaleTimeString()+'>___XXX___SpiderManager: Added url to LOG in DB.');
                        }
                    });

                } else {
                    time = new Date();
                    console.log('<' + time.toLocaleTimeString() + '>___XXX___SpiderManager: UPDATING into log:' + newLog.url + ' ' + newLog.timeScanned);

                    db.collection('log1').update({url: {$eq: newLog.url}}, newLog, function (err, result) {
                        if (err) {
                            time = new Date();
                            console.log('<' + time.toLocaleTimeString() + '>___OOO___SpiderManager: Could not add URL to DB LOG.');
                        } else {
                            time = new Date();
                            //console.log('<'+time.toLocaleTimeString()+'>___XXX___SpiderManager: Added url to LOG in DB.');
                        }
                    });
                }

            }
        });
    }

    function putToQue(newURL) {
        db.collection('que1').findOne({url: {$eq: newURL.url}}, function (err, result) {
            if (err) {
                time = new Date();
                console.log('<' + time.toLocaleTimeString() + '>___OOO___SpiderManager: Problem reading url');
            } else {
                if (result == null) {
                    db.collection('que1').insert(newURL, function (err, result) {
                        if (err) {
                            time = new Date();
                            console.log('<' + time.toLocaleTimeString() + '>___OOO___SpiderManager: Could not add URL to DB que.');
                        } else {
                            time = new Date();
                            console.log('<' + time.toLocaleTimeString() + '>___XXX___SpiderManager: Added url to que in DB:' + newURL.url);
                        }
                    });
                }

            }
        });

    }

    function getFromQue(newUrl) {


    }

    function getFromLogOldest() {
        time = new Date();
        db.collection('log1').findOne({timeScanned: {$lt: time.getTime() - expirationTime * 60 * 1000}}, function (err, result) {
            if (err) {
                time = new Date();
                console.log('<' + time.toLocaleTimeString() + '>___OOO___SpiderManager: could not POP url from LOG!');
                next = home;
            } else {
                if (result == null) {
                    time = new Date();
                    console.log('<' + time.toLocaleTimeString() + '>___XXX___SpiderManager: LOG empty!');
                    next = home;
                } else {
                    console.log('<' + time.toLocaleTimeString() + '>___XXX___SpiderManager: got url from LOG:' + result.url + '->' + result.timeScanned);

                    time = new Date();
                    let newLog = {
                        url: result.url,
                        timeScanned: time.getTime()
                    };

                    putToLog(newLog);
                    next = result.url;
                }

            }


        });
    }

    function popfromQue() {

        db.collection('que1').findOne({}, function (err, result) {

            if (err) {

                time = new Date();
                console.log('<' + time.toLocaleTimeString() + '>___OOO___SpiderManager: could not POP url from que!');
                next = home;
            } else {
                if (result == null) {
                    time = new Date();
                    console.log('<' + time.toLocaleTimeString() + '>___XXX___SpiderManager: QUE empty!');
                    getFromLogOldest();
                } else {
                    db.collection('que1').remove({url: {$eq: result.url}});
                    console.log('<' + time.toLocaleTimeString() + '>___XXX___SpiderManager: removed from que:' + result.url);
                    next = result.url;
                }
            }
        });


    }

    setInterval(function () {
        popfromQue();
        time = new Date();
        let newLog = {
            url: next,
            timeScanned: time.getTime()
        };
        putToLog(newLog);
        sessionLogs++;
        console.log('<' + time.toLocaleTimeString() + '>___---___SpiderManager: GOTO<' + sessionLogs + '>:' + next);
        mq.emit('goto1', next);


    }, spiderCoolOff * 1000); // 3s space-between

    mq.on('discover1', function (newUrl) {
        //time=new Date();
        //console.log('<'+time.toLocaleTimeString()+'>___+++___SpiderManager: Discovered new URL:'+newUrl);
        newUrl = newUrl.replace(/\?recid=.{1,}/g, "");
        newUrl = newUrl.replace(/\?ref=.{1,}/g, "");
        newUrl = newUrl.replace(/^\/.{1,}/g, "");
        if (newUrl == "") {
            return;
        }
        time = new Date();
        console.log('<' + time.toLocaleTimeString() + '>___+++___SpiderManager: Discovered new URL:' + newUrl);


        //adding url to que in the DB
        var newURL = {
            url: newUrl,
            timeDiscovered: time.getTime()
        };


        //get from que
        db.collection('que1').findOne({url: {$eq: newUrl}}, function (err, result) {
            if (err) {
                time = new Date();
                console.log('<' + time.toLocaleTimeString() + '>___OOO___SpiderManager: Problem reading url');

            } else {
                if (result == null) {


                    db.collection('log1').findOne({url: {$eq: newUrl}}, function (err, result) {
                        if (err) {
                            time = new Date();
                            console.log('<' + time.toLocaleTimeString() + '>___OOO___SpiderManager: Problem reading url');
                        } else {

                            if (result != null) {
                                time = new Date();
                                console.log('<' + time.toLocaleTimeString() + '>___^^^___SpiderManager: CHECKING TIME!' + (time.getTime() - result.timeScanned).toString());
                                if ((time.getTime() - result.timeScanned) > (expirationTime * 60 * 1000)) {
                                    console.log('<' + time.toLocaleTimeString() + '>___(((___SpiderManager: url scanned long ago, ADDING!');
                                    putToQue(newURL);
                                } else {
                                    console.log('<' + time.toLocaleTimeString() + '>___RRR___SpiderManager: url scanned RECENTLY!');
                                }
                            } else {
                                time = new Date();
                                console.log('<' + time.toLocaleTimeString() + '>___+++___SpiderManager: adding URL!');

                                putToQue(newURL);
                            }

                        }
                    });


                }
            }
        });


    });

};