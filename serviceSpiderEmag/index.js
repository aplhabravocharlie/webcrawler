
//
// Descriere serviciu spider emag.
//

// Declaram resursele folosite de acest serviciu.
let Nightmare = require('nightmare');
let cheerio = require('cheerio');
let db;
let mq;
let monit;
let browser;
let LZString = require('lz-string');
//
let title = 'crawler-emag-'+Math.floor((Math.random()*10e8-10e7)+10e7);

// Configuratii si dependente.
// Mentionam proprietatea port pentru portul folosit de serviciu.
// Proprietatea port este necesara doar daca folosesti bibliotecile common http sau rtc.
// Dependencies mentioneaza care dintre bibliotecile common sunt folosite.
module.exports.conf = {
    port: 8080,             // portul pe care porneste serviciul (optional aici)
    dependencies: [
        '../common/db',     // folosim baza de date
        '../common/mq',     // folosim canalul de mesaje
    ]
};

// Functia de initializare a serviciului.
// Daca initializarea a esuat, chemam cbFail cu mesajul de eroare.
// Daca initializarea a reusit, chemam cbSuccess.
// Aceasta functie este teoretic optionala, nu este necesar sa o definesti. Practic
// insa, vom initializa cel putin mq in toate serviciile.
module.exports.init = function(cbSuccess, cbFail) {

    console.log("Service is starting...");

    // Monitorizare
    monit = require('../common/monit').getSocket();

    // Inregistrare.
    monit.emit('register-service', {
        title: title,
        ip: require('../common/monit').getIp(),
        type:"crawler",
        minMemoryValue:"0",
        maxMemoryValue:"100", 
        actualMemoryValue:(Math.random()*100-40)+40,
        minConnectionValue:"0", 
        maxConnectionValue:"100", 
        actualConnectionValue: (Math.random()*100-40)+40, 
        state:"active"
    });

    // Preluam resursele pe care le-am mentionat.
    db = require('../common/db').getClient();
    mq = require('../common/mq').getClient();

    // Initializam browser-ul.
    browser = Nightmare();

    // Anuntam ca am terminat initializarea cu succes.
    cbSuccess();

}

// Functia de oprire a serviciului.
// Dupa ce am curatat ce era necesar, chemam functia exit.
// Nu avem voie sa aruncam erori aici.
// Nu putem opri incheierea serviciului.
// Aceasta functie este optionala, nu este necesar sa o definesti. Este
// prezenta aici doar ca exemplu.
module.exports.final = function(exit) {

    console.log("Service is stopping...");

    // De-Inregistrare.
    monit.emit('deregister-service', {
        title: title,
        ip: require('../common/monit').getIp(),
        type:"crawler",
        minMemoryValue:"0",
        maxMemoryValue:"100", 
        actualMemoryValue:(Math.random()*100-40)+40,
        minConnectionValue:"0", 
        maxConnectionValue:"100", 
        actualConnectionValue: (Math.random()*100-40)+40, 
        state:"active"
    });

    // Nu avem ce curata.
    exit();

}

// Functia de rulare permanenta a serviciului.
// Aici va puteti atasa la canalele de mesaje.
module.exports.run = function() {

    // directive:
    // mesajul de tip 'process' nu mai este necesar
    // inainte erau 2 servicii, acum e doar 1
    //
    // comunicati cu echipa serviceSpiderManager pentru a afla
    // ce mesaje trebuie preluate
    //
    // in codul de mai jos aveti exact ce era inainte
    // (mai putin o redenumire a variabilei service->mq)
    // deci va puteti folosi de el pentru a va orienta cum
    // sa folositi baza de date, etc

    // atentie!
    // cand trimiteti mesaje, este recomandat sa folositi obiecte
    // nu primitive
    // DA: mq.emit('mesaj', { url: "gfdglfgldfmgl" })
    // NU: mq.emit('mesaj', "gfdglfgldfmgl")
    
    // biblioteci: (mai usor de goagale)
    //      cheerio = parser HTML; puteti folosi selectori CSS pentru a selecta info
    //      db & mongodb = client bd MongoDB
    //      Nightmare = browser virtual; folosit pentru a prelua pagini web

    // vechiul spider
    mq.on('goto1', function(url) {
        let time=new Date();
        console.log('<'+time.toLocaleTimeString()+'>___???___Spider: Extracting data.');
    
        browser.goto(url).wait(2000)
        .evaluate(() => document.body.innerHTML)
        .then((data) => {
            mq.emit('process1', {
                url: url,
                timestamp: new Date(),
                payload: data
            });
        });
    });   

    // vechiul processor
    mq.on('process1', function(container) {

        let time;
        const dom = cheerio.load(container.payload);
        var comments = [];
        let cheerioTableparser = require('cheerio-tableparser');

        // Check if a product page.
        if (dom('.product-code-display') != null) {
            time=new Date();
            console.log('<'+time.toLocaleTimeString()+'>___VVV___Spider: New product found.');

            cheerioTableparser(dom);
            var data = dom('.table-responsive .product-page-specifications tbody').parsetable(true, true, true);

            dom('.product-review-body .pad-btm-xs').each(function(i, elem) {
                var comment = '';
                for(var i=0; i<elem.children.length; i++) {
                    comment = comment + elem.children[i].data;
                }
                // comment refine
                comment=comment.replace(/\t/g,'');
                comment=comment.replace(/  +/g, ' ');
                //console.log(comment);
                comments.push(comment);
            });

            // Extract product data.
            let newProduct = {
                a: "eMag",  //store
                b: dom('.page-title').text().trim(),    //title
                c: dom('.product-page-description-text').text().trim(),   //description
                d: dom('.product-highlight .product-new-price').text().trim(),  //price
                e: container.url, //url
                f: dom('.product-highlight .star-rating-text').text().trim(),  //rating
                g: dom('.product-code-display').text().trim(),   //code
                h: dom('.product-gallery-inner .product-gallery-image').find('img').attr('src'),    //image
                i: JSON.stringify(data),   //specifications
                j: comments,    //comments
                k: time.getDate()    //timestamp
                }

            // Edit product data

            //Deleting \t's
            newProduct.c=newProduct.c.replace(/\t/g,'');
            
            //Retriming
            newProduct.c=newProduct.c.replace(/  +/g, ' ');

            //Product code refining
            newProduct.g = newProduct.g.substring(12);

            //Price refining
            newProduct.d =newProduct.d.slice(0,-6);
            newProduct.d = parseFloat(newProduct.d);

            //Rating refining
            newProduct.f = parseFloat(newProduct.f);

            //Compressing data
            newProduct.c=LZString.compress(newProduct.c);
            newProduct.i=LZString.compress(newProduct.i);
            for(var i=0; i<newProduct.j.length; i++) {
                newProduct.j[i] = LZString.compress(newProduct.j[i]);
            }


            //console.log(JSON.parse(LZString.decompress(newProduct.i)))


            // Test extraction
            /*
            console.log(newProduct.b);
            console.log(newProduct.g);
            console.log(newProduct.d);
            console.log(newProduct.f);
            console.log(newProduct.h);
            console.log(newProduct.i);
            console.log(newProduct.j);
            */
    
            // Persist data.
            db.collection('products').findOne({g: { $eq: newProduct.g}}, function(err, result) {
                if(result != null) {
                    time=new Date();
                    console.log('<'+time.toLocaleTimeString()+'>___<<<___Spider: Product already exists in database with code: ' + result._id);
                    if(time.getDate() > newProduct.k) {
                        console.log('<'+time.toLocaleTimeString()+'>___<<<___Spider: Replacing old information.');
                        db.collection('products').remove({g:newProduct.g},function(err,result){
                            if (err) {
                                time=new Date();
                                console.log('<'+time.toLocaleTimeString()+'>___VVV___Spider: Could not persist product removal.');
                            }
                        });
                        db.collection('products').update( { g:newProduct.g },newProduct,function(err,result){
                            if (err) {
                                time=new Date();
                                console.log('<'+time.toLocaleTimeString()+'>___VVV___Spider: Could not persist product.');
                            } else {
                                // Announce other services.
                                mq.emit('new-product1', newProduct);
                            }
                        });
                    }
                } else {
                    if(newProduct.g!="") {
                        db.collection('products').insert(newProduct, function(err, result) {
                            if (err) {
                                time=new Date();
                                console.log('<'+time.toLocaleTimeString()+'>___VVV___Spider: Could not persist product.');
                            } else {
                                // Announce other services.
                                mq.emit('new-product1', newProduct);
                            }
                        });
                    }   
                }
            })
        }
        else {
            time=new Date();
            console.log('<'+time.toLocaleTimeString()+'>___XXX___Spider: Not a product.');
        }
    
        // Check for new links.
        dom('.card-item .card').each(function(i, elem) {
            let url = dom(this).attr('href');
            if (typeof url !== 'undefined') {
                mq.emit('discover1', url);
                //console.log(url);
            }
        });
    
    });    

}
