//
// Serviciu interfata.
// Heroku:
// Name: Proiect Securitate (first last)
// Email: florica.cosmin@yahoo.com
// Pass: Herokupassword1$
//

// Declaram resursele folosite de acest serviciu.
let db;
let mq;
let broadcast = null;
let path = require('path');
let bodyParser = require('body-parser');
let LZString = require('lz-string');
//
let monitors = [];

// Configuratii si dependente.
// Mentionam proprietatea port pentru portul folosit de serviciu.
// Proprietatea port este necesara doar daca folosesti bibliotecile common http sau rtc.
// Dependencies mentioneaza care dintre bibliotecile common sunt folosite.
module.exports.conf = {
    port: process.env.PORT || 8080,             // portul pe care porneste serviciul
    dependencies: [
        '../common/db',     // folosim baza de date
        '../common/mq',     // folosim canalul de mesaje
        '../common/http',   // folosim server HTTP
        '../common/rtc',    // folosim server RTC
    ]
};

// Functia de initializare a serviciului.
// Daca initializarea a esuat, chemam cbFail cu mesajul de eroare.
// Daca initializarea a reusit, chemam cbSuccess.
// Aceasta functie este teoretic optionala, nu este necesar sa o definesti. Practic
// insa, vom initializa cel putin mq in toate serviciile.
module.exports.init = function(cbSuccess, cbFail) {

    console.log("Service is starting...");

    // Preluam resursele pe care le-am mentionat.
    db = require('../common/db').getClient();
    mq = require('../common/mq').getClient();
    app = require('../common/http').getServer();
    io = require('../common/rtc').getServer();

    // vechiul ui
    // Save for later.
    broadcast = io;

    // Manage requests.
    app.use(bodyParser.json());

    app.use('/public', require('express').static(
        path.join(__dirname, 'public')
    ));

    app.get('/monit', function(req, res) {
        res.status(200).sendFile(path.join(__dirname, 'public', 'index.html'));
    })

    app.post('/search', function(req, res) {
        console.log(req.body.tags);
        let tags = req.body.tags || [];
        let query = { b: new RegExp(tags.join("|"), "i") }
        let options = { limit: 10, skip: req.body.skip || 0 };

        // Echo.
        console.log(query, options);

        db.collection('products').find(query, options).toArray(function(err, result) {
            if (err) {
                console.log('UI: Could not retrieve products from database.');
                res.status(404).json([]);
            } else {
                // Construire obiect.
                let procResult = result.map((item) => {
                    let specificatii = LZString.decompress(item.g);

                    try {
                        specificatii = JSON.parse(specificatii);
                    } catch (err) {
                        specificatii = {};
                    }

                    return {
                        dbId: item["_id"],
                        magazin: item.a,
                        cod: item.g,
                        titlu: item.b,
                        descriere: LZString.decompress(item.c),
                        price: item.d,
                        url: item.e,
                        rating: item.f,
                        img: item.h,
                        specificatii: specificatii,
                        comentarii: item.j.map((comen) => LZString.decompress(comen)),
                        timestamp: item.k,
                    };
                });

                res.status(200).json(procResult);
            }
        });
    })

    // Manage connections.
    io.on('connection', function(socket) {

        time=new Date();
        console.log('<'+time.toLocaleTimeString()+'>___"""___UI: New client has connected to the server.');

        socket.on('disconnect', function(socket) {

            time=new Date();
            console.log('<'+time.toLocaleTimeString()+'>___***___UI: A client has disconnected from the server.');
        });

        // Relay.
        socket.emit('initial-service-list', monitors);
        socket.on('register-service', (data) => {
            monitors.push(data);
            broadcast.emit('register-service', data);
        });
        socket.on('deregister-service', (data) => {
            monitors = monitors.filter((item) => {
                return item.title.trim().toUpperCase() !== data.title.trim().toUpperCase();
            });
            broadcast.emit('deregister-service', data);
        });
        socket.on('status-service', (data) => {
            monitors = monitors.map((item) => {
                if (item.ip === data.ip)
                    return data;
                else
                    return item;
            });
            broadcast.emit('status-service', data);
        });
    });

    // Anuntam ca am terminat initializarea cu succes.
    cbSuccess();

}

// Functia de oprire a serviciului.
// Dupa ce am curatat ce era necesar, chemam functia exit.
// Nu avem voie sa aruncam erori aici.
// Nu putem opri incheierea serviciului.
// Aceasta functie este optionala, nu este necesar sa o definesti. Este
// prezenta aici doar ca exemplu.
module.exports.final = function(exit) {

    console.log("Service is stopping...");

    // Nu avem ce curata.
    exit();

}

// Functia de rulare permanenta a serviciului.
// Aici va puteti atasa la canalele de mesaje.
module.exports.run = function() {

    // vechiul ui
    // Manage product events.
    mq.on('new-product', function(newProduct) {
        broadcast.emit('new-product', newProduct);

        time=new Date();
        console.log('<'+time.toLocaleTimeString()+'>___^^^___UI: A product event has been dispatched to all clients.');
    });

}
