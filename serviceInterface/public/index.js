let root = document.getElementById('list');

let services = [];

function addListService(services) {
    let html='';
    if(services.constructor!==Array){
        services=[services];
    }

    if (services.length === 0) {
        root.innerHTML = '';
        return;
    }
    
    services.forEach(function(service){
        if(typeof service.title==='undefinded'|| 
           typeof service.ip==='undefined' ||
           typeof service.type==='undefined' ||
           typeof service.minMemoryValue==="undefined" ||
           typeof service.maxMemoryValue==="undefined" ||
           typeof service.actualMemoryValue==="undefined" ||
           typeof service.minConnectionValue==="undefined" ||
           typeof service.maxConnectionValue==="undefined" ||
           typeof service.actualConnectionValue==="undefined" ||
           typeof service.state==="undefined"
          ){
            console.log('Error: Bad service');
            return;
        }
        html+=`
            <li>
                <div id="single-div"><span>${service.title}</span></div>
                <div id="single-div"><span>${service.ip}</span></div>
                <div id="single-div">
                    <Small>Used memory</Small>
                    <div class="progress">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuemin="${service.minMemoryValue}" aria-valuemax="${service.maxMemoryValue}" style="width:${service.actualMemoryValue}%">${service.actualMemoryValue}%</div>
                        </div>
                    </div>
                <div id="single-div">
                    <Small>Active connections</Small>
                    <div class="progress">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuemin="${service.minConnectionValue}" aria-valuemax="${service.maxConnectionValue}" style="width:${service.actualConnectionValue}%">${service.actualConnectionValue}%</div>
                        </div>
                    </div>
                <div id="single-div">
                    <p> <span>${service.state} </span></p>
                </div>
                <div id="single-div">
                    <img src="public/resources/${service.type}.png">
                </div>
            </li>
        `;
    });
    root.innerHTML=html;
}

let domain = 'https://proiectsecuritate2018.herokuapp.com/';
let socket = io(domain, {
    transports: ['websocket'],
    upgrade: false
});

socket.on('register-service', function(newService) {
    services.push(newService);
    addListService(services);
});
socket.on('deregister-service', function(newService) {
    services = services.filter((item) => {
        return item.title.trim().toUpperCase() !== newService.title.trim().toUpperCase();
    });
    addListService(services);
});
socket.on('status-service', function(reService) {
    services = services.map((item) => {
        if (item.ip === reService.ip)
            return reService;
        else
            return item;
    });
    addListService(services);
});

socket.on('initial-service-list', function(initServices) {
    services = initServices;
    addListService(services);
});

//service.type - pentru icon   crawler / emag / heroku / manager / mongodb / pcgarrage / rabbitmq