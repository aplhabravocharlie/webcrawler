//
// Descriere serviciu spider emag.
//

// Declaram resursele folosite de acest serviciu.
let Nightmare = require('nightmare');
let cheerio = require('cheerio');
let db;
let mq;
let browser;

// Configuratii si dependente.
// Mentionam proprietatea port pentru portul folosit de serviciu.
// Proprietatea port este necesara doar daca folosesti bibliotecile common http sau rtc.
// Dependencies mentioneaza care dintre bibliotecile common sunt folosite.
module.exports.conf = {
    port: 8080,             // portul pe care porneste serviciul (optional aici)
    dependencies: [
        '../common/db',     // folosim baza de date
        '../common/mq',     // folosim canalul de mesaje
    ]
};

// Functia de initializare a serviciului.
// Daca initializarea a esuat, chemam cbFail cu mesajul de eroare.
// Daca initializarea a reusit, chemam cbSuccess.
// Aceasta functie este teoretic optionala, nu este necesar sa o definesti. Practic
// insa, vom initializa cel putin mq in toate serviciile.
module.exports.init = function(cbSuccess, cbFail) {

    console.log("Service is starting...");

    // Preluam resursele pe care le-am mentionat.
    db = require('../common/db').getClient();
    mq = require('../common/mq').getClient();

    // Initializam browser-ul.
    browser = Nightmare();

    // Anuntam ca am terminat initializarea cu succes.
    cbSuccess();

}

// Functia de oprire a serviciului.
// Dupa ce am curatat ce era necesar, chemam functia exit.
// Nu avem voie sa aruncam erori aici.
// Nu putem opri incheierea serviciului.
// Aceasta functie este optionala, nu este necesar sa o definesti. Este
// prezenta aici doar ca exemplu.
module.exports.final = function(exit) {

    console.log("Service is stopping...");

    // Nu avem ce curata.
    exit();

}

// Functia de rulare permanenta a serviciului.
// Aici va puteti atasa la canalele de mesaje.
module.exports.run = function() {

    // directive:
    // mesajul de tip 'process' nu mai este necesar
    // inainte erau 2 servicii, acum e doar 1
    //
    // comunicati cu echipa serviceSpiderManager pentru a afla
    // ce mesaje trebuie preluate
    //
    // in codul de mai jos aveti exact ce era inainte
    // (mai putin o redenumire a variabilei service->mq)
    // deci va puteti folosi de el pentru a va orienta cum
    // sa folositi baza de date, etc

    // atentie!
    // cand trimiteti mesaje, este recomandat sa folositi obiecte
    // nu primitive
    // DA: mq.emit('mesaj', { url: "gfdglfgldfmgl" })
    // NU: mq.emit('mesaj', "gfdglfgldfmgl")
    
    // biblioteci: (mai usor de goagale)
    //      cheerio = parser HTML; puteti folosi selectori CSS pentru a selecta info
    //      db & mongodb = client bd MongoDB
    //      Nightmare = browser virtual; folosit pentru a prelua pagini web

    // vechiul spider
    mq.on('goto2', function(url) {
        let time=new Date();
        console.log('<'+time.toLocaleTimeString()+'>___???___Spider: Extracting data.');
    
        browser.goto(url).wait(2000)
        .evaluate(() => document.body.innerHTML)
        .then((data) => {
            mq.emit('process2', {
                url: url,
                timestamp: new Date(),
                payload: data
            });
        });
    });   

    // vechiul processor
    mq.on('process2', function(container) {

        let time;
        const dom = cheerio.load(container.payload);

        // Check if a product page.
        if (dom('.product-code-display') != null) {
            time=new Date();
            console.log('<'+time.toLocaleTimeString()+'>___VVV___Spider: New product found.');
            let proprieties = [];
            // Extract product data.
            let newProduct = {
                title: dom('.p-name').text().trim(),
                description: dom('.pe-content td p').text().trim(),
                price: dom('.ps-sell-price span').text().trim(),
                url: container.url,
                rating: dom('.ar-title span').text().trim(),
                code: dom('.owncode span').text().trim(),
                image: dom('..pm-image img').findOne('img').attr('src'), 
                specifications: dom('.specs-table tr').text().trim(),
                }
            
            // Edit product data
            newProduct.code = newProduct.code.substring(12);
            newProduct.price =newProduct.price.substring(0, newProduct.price.length-4);

            // Test extraction
            console.log(newProduct.title);
            console.log(newProduct.code);
            console.log(newProduct.price);
            console.log(newProduct.rating);
            console.log(newProduct.image);
            console.log(newProduct.specifications);
    
            // Persist data.
            db.collection('products').findOne({url: { $eq: newProduct.url}}, function(err, result) {
                if(result != null) {
                    time=new Date();
                    console.log('<'+time.toLocaleTimeString()+'>___<<<___Spider: Product already exists in database.');
                } else {
                    if(newProduct.code!="") {
                        db.collection('products').insert(newProduct, function(err, result) {
                            if (err) {
                                time=new Date();
                                console.log('<'+time.toLocaleTimeString()+'>___VVV___Spider: Could not persist product.');
                            } else {
                                // Announce other services.
                                mq.emit('new-product2', newProduct);
                            }
                        });
                    }   
                }
            })
        }
        else {
            time=new Date();
            console.log('<'+time.toLocaleTimeString()+'>___XXX___Spider: Not a product.');
        }
    
        // Check for new links.
        dom('.card-item .card').each(function(i, elem) {
            let url = dom(this).attr('href');
            if (typeof url !== 'undefined') {
                mq.emit('discover2', url);
            }
        });
    
    });    

}
