//
// Acesta este un serviciu generic.
// Lasati in comentariul asta de sus niste documentatie
// despre cum merge serviciul respectiv / ce responsabilitati
// are.
//

// Declaram resursele folosite de acest serviciu.
let db;
let mq;

// Configuratii si dependente.
// Mentionam proprietatea port pentru portul folosit de serviciu.
// Proprietatea port este necesara doar daca folosesti bibliotecile common http sau rtc.
// Dependencies mentioneaza care dintre bibliotecile common sunt folosite.
module.exports.conf = {
    port: 8080,             // portul pe care porneste serviciul (optional aici)
    dependencies: [
        '../common/db',     // folosim baza de date
        '../common/mq',     // folosim canalul de mesaje
    ]
};

// Functia de initializare a serviciului.
// Daca initializarea a esuat, chemam cbFail cu mesajul de eroare.
// Daca initializarea a reusit, chemam cbSuccess.
// Aceasta functie este teoretic optionala, nu este necesar sa o definesti. Practic
// insa, vom initializa cel putin mq in toate serviciile.
module.exports.init = function(cbSuccess, cbFail) {

    console.log("Service is starting...");

    // Preluam resursele pe care le-am mentionat.
    db = require('../common/db').getClient();
    mq = require('../common/mq').getClient();

    // Anuntam ca am terminat initializarea cu succes.
    cbSuccess();

}

// Functia de oprire a serviciului.
// Dupa ce am curatat ce era necesar, chemam functia exit.
// Nu avem voie sa aruncam erori aici.
// Nu putem opri incheierea serviciului.
// Aceasta functie este optionala, nu este necesar sa o definesti. Este
// prezenta aici doar ca exemplu.
module.exports.final = function(exit) {

    console.log("Service is stopping...");

    // Nu avem ce curata.
    exit();

}

// Functia de rulare permanenta a serviciului.
// Aici va puteti atasa la canalele de mesaje.
module.exports.run = function() {

    // Alt cod ce ruleaza in fundal la un interval de 1s.
    setInterval(function() {
        console.log("1sec has passed..");
    }, 1 * 1000);

    // Ascultare mesaje de un anumit tip.
    mq.on('eveniment.generic.tip1', function() {
        
        console.log('Received generic event type 1');

        // Trimitere mesaje cu argumente.
        mq.emit('eveniment.generic.tip2', {
            date: ".admlsflasdmkflasdmflew"
        });

    });

    // Alt tip de mesaj.
    mq.on('eveniment.generic.tip2', function(datePrimite) {
        
        console.log('Received generic event type 2 with content ', datePrimite.date);

        // Trimitere mesaje fara argumente.
        mq.emit('eveniment.generic.tip3');

    });

}
