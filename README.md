1. Instalare Git.
Verifica prin:
```bash
    git --version
```

2. Instalare NodeJS si NPM.
Verifica prin:
```bash
    node -v
    npm -v
```

3. Instalare proiect
```bash
    cd common
    npm install

    cd serviciulDorit
    npm install
```

4. Rulare proiect
```bash
    cd serviciulDorit
    npm start
```
