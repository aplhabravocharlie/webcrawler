let domain = 'https://proiectsecuritate2018.herokuapp.com/';

let tags = require('./components/tags');
let list = require('./components/list');
let active = require('./components/active');

let shell = require('electron').shell;
let remote = require('electron').remote;
let io = require('socket.io-client')(domain, {
    transports: ['websocket'],
    upgrade: false
});

// Handle external links.
document.addEventListener('click', function(event) {
    if (typeof event.target.href === 'undefined')
        return;

    if (event.target.href.startsWith('http')) {
        event.preventDefault();
        shell.openExternal(event.target.href);
    }
});

// Handle title bar buttons.
let buttonClose = document.getElementById('button-close');
let buttonOnline = document.getElementById('button-online');
let buttonExit = document.getElementById('button-exit');

buttonClose.addEventListener('click', () => remote.getCurrentWindow().close());

// Handle online status events.
io.on('connect', function() {
    buttonOnline.className = 'online';

    if (list.getActivePane() === 'cantsearch')
        list.changeActivePane('nosearch');
});
io.on('disconnect', function() {
    buttonOnline.className = '';

    if (active.getCurrActiveProduct() === null)
        list.changeActivePane('cantsearch');
});

// Product events.
//io.on('new-product', addNewProduct);
