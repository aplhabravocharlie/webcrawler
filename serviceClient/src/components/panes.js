let active = require('./active');
let details = require('./details');

// Bindings.
let searchPane = document.getElementById('search');
let cardPane = document.getElementById('card');
let buttonExit = document.getElementById('button-exit');
let searchList = document.getElementById('searchlist').getElementsByTagName('li');

// State.
let searchPaneActive = false;
let cardPaneActive = false;

let changeSearchPaneState = function(newState) {
    if (newState === true) {
        searchPaneActive = true;
        buttonExit.className = '';
    } else {
        searchPaneActive = false;
        buttonExit.className = 'hidden';
    }
}

let changeCardPaneState = function(newState) {
    cardPaneActive = newState;
}

// t = timp actual, b = valoare baza, c = ritm schimbare, d = durata
function easeInOut(t, b, c, d) {
    if ((t /= d / 2) < 1) return c / 2 * t * t * t * t + b;
    return -c / 2 * ((t -= 2) * t * t * t - 2) + b;
}

// Handle enter-exit animations.
let searchPaneEnterAnimation = function(next) {
    if (searchPaneActive)
        return;

    let from = 100;
    let to = 30;
    let duration = 500; //500ms

    // Start of animation.
    let start = new Date().getTime();
    let timer = setInterval(function() {
        // Current frame.
        let time = new Date().getTime() - start;
        let width = easeInOut(time, from, to - from, duration);
        searchPane.style.width = width + '%';

        // Adapt elements.
        for (var i = 0; i < searchList.length; i++) {
            if (searchList[i].querySelectorAll('.cc-1').length === 0)
                continue;

            if ((duration - time) / duration < 0.4)
                searchList[i].querySelector('.cc-1').style.display = 'none';
            if ((duration - time) / duration < 0.8)
                searchList[i].querySelector('.cc-3').style.display = 'none';
            if ((duration - time) / duration < 0.7)
                searchList[i].querySelector('.cc-4').style.display = 'none';
        }

        // End of animation.
        if (time >= duration) {
            clearInterval(timer);
            changeSearchPaneState(true);
            // Next animation.
            if (typeof next !== 'undefined')
                next();
        }
    }, 1000 / 60); //60fps
}

let searchPaneExitAnimation = function(next) {
    if (!searchPaneActive)
        return;

    let from = 30;
    let to = 100;
    let duration = 500; //500ms

    // Start of animation.
    let start = new Date().getTime();
    let timer = setInterval(function() {
        // Current frame.
        let time = new Date().getTime() - start;
        let width = easeInOut(time, from, to - from, duration);
        searchPane.style.width = width + '%';

        // Adapt elements.
        for (var i = 0; i < searchList.length; i++) {
            if (searchList[i].querySelectorAll('.cc-1').length === 0)
                continue;
                
            if (time / duration > 0.4)
                searchList[i].querySelector('.cc-1').style.display = 'inline-block';
            if (time / duration > 0.8)
                searchList[i].querySelector('.cc-3').style.display = 'inline-block';
            if (time / duration > 0.7)
                searchList[i].querySelector('.cc-4').style.display = 'inline-block';
        }

        // End of animation.
        if (time >= duration) {
            clearInterval(timer);
            changeSearchPaneState(false);
            // Next animation.
            if (typeof next !== 'undefined')
                next();
        }
    }, 1000 / 60); //60fps
}

let cardPaneEnterAnimation = function(next) {
    if (cardPaneActive)
        return;

    let from = 100;
    let to = 0;
    let duration = 500; //500ms

    // Start of animation.
    let start = new Date().getTime();
    let timer = setInterval(function() {
        // Current frame.
        let time = new Date().getTime() - start;
        let top = easeInOut(time, from, to - from, duration);
        cardPane.style.top = top + '%';

        // End of animation.
        if (time >= duration) {
            clearInterval(timer);
            changeCardPaneState(true);
            // Next animation.
            if (typeof next !== 'undefined')
                next();
        }
    }, 1000 / 60); //60fps
}

let cardPaneExitAnimation = function(next) {
    if (!cardPaneActive)
        return;

    let from = 0;
    let to = 100;
    let duration = 500; //500ms

    // Start of animation.
    let start = new Date().getTime();
    let timer = setInterval(function() {
        // Current frame.
        let time = new Date().getTime() - start;
        let top = easeInOut(time, from, to - from, duration);
        cardPane.style.top = top + '%';

        // End of animation.
        if (time >= duration) {
            clearInterval(timer);
            changeCardPaneState(false);
            // Next animation.
            if (typeof next !== 'undefined')
                next();
        }
    }, 1000 / 60); //60fps
}

// Handle composite animations.
let lockAnimation = false;

let openPanes = (next) => {
    if (lockAnimation)
        return;

    lockAnimation = true;
    searchPaneEnterAnimation(() => {
        details.renderProduct(active.getCurrActiveProductData());
        cardPaneEnterAnimation(() => {
            lockAnimation = false;
            if (typeof next !== 'undefined') next();
        });
    });
};

let closePanes = (next) => {
    if (lockAnimation)
        return;

    lockAnimation = true;
    if (cardPaneActive) {
        cardPaneExitAnimation(() => {
            details.renderProduct(null);
            searchPaneExitAnimation(() => {
                lockAnimation = false;
                if (typeof next !== 'undefined') next();
            });
        });
    } else {
        searchPaneExitAnimation(() => {
            if (cardPaneActive)
                cardPaneExitAnimation(() => {
                    details.renderProduct(null);
                    lockAnimation = false;
                    if (typeof next !== 'undefined') next();
                });
        });
    }
};

let switchPanes = (next) => {
    if (lockAnimation)
        return;

    cardPaneExitAnimation(() => {
        details.renderProduct(active.getCurrActiveProductData());
        cardPaneEnterAnimation(() => {
            lockAnimation = false;
            if (typeof next !== 'undefined') next();
        });
    });
}

// Export data.
module.exports = {
    searchPane,
    cardPane,
    searchPaneEnterAnimation,
    searchPaneExitAnimation,
    cardPaneEnterAnimation,
    cardPaneExitAnimation,
    openPanes,
    closePanes,
    switchPanes
}
