let panes = require('./panes');
let details = require('./details');
let active = require('./active');

// Bindings.
let searchBody = document.getElementById('searchbody');
//
let scrollList = document.getElementById('searchlist');
let scrollLeft = document.querySelector('.uk-pagination-previous');
let scrollRight = document.querySelector('.uk-pagination-next');
let buttonMore = document.getElementById('more');
let buttonSearch = document.getElementById('searchbar-button');
let buttonExit = document.getElementById('button-exit');

// State.
let productList = [];
let moreAvailable = false;
//
let activePane = 'cantsearch';

// Search actions.
let lockAnimation = false;
buttonExit.addEventListener('click', () => {
    if (lockAnimation)
        return;

    lockAnimation = true;
    panes.closePanes(() => {
        lockAnimation = false;
        active.changeActiveProduct(null, null);
    });
});

buttonSearch.addEventListener('click', search);

function sendRequest(tags) {
    let xhttp = new XMLHttpRequest();
    xhttp.open('POST', domain + 'search', true);
    xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhttp.onreadystatechange = function () {
        if (this.readyState === 4) {
            if (this.status === 200) {
                // Data.
                productList = JSON.parse(this.responseText);
                moreAvailable = true;
            
                // Update.
                if (typeof productList === 'undefined' || productList === null || productList.length === 0) {
                    changeActivePane('emptysearch');
                } else {
                    viewList();
                    changeActivePane('searchresult');
                }
            }
            else changeActivePane('errorsearch');
        }
    };
    xhttp.send(JSON.stringify({tags:tags, skip:0}));

    changeActivePane('waitsearch');
}

function search() {
    // Check if online.
    if (document.getElementById('button-online').className !== 'online')
        return;

    let tags = [];
    this.parentElement.querySelectorAll('.tag').forEach(i => tags.push(i.dataset.tag));

    // Exit current selection.
    if (active.getCurrActiveProduct() !== null) {
        if (lockAnimation) return;
        lockAnimation = true;
        panes.closePanes(() => {
            lockAnimation = false;
            active.changeActiveProduct(null, null);
    
            // Actual search.
            sendRequest(tags);
        });
    }
    else sendRequest(tags);
}

function more() {
    // Check if online.
    if (document.getElementById('button-online').className !== 'online')
        return;

    let tags = [];
    this.parentElement.querySelectorAll('.tag').forEach(i => tags.push(i.dataset.tag));

    let xhttp = new XMLHttpRequest();
    xhttp.open('POST', domain + 'search', true);
    xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhttp.onreadystatechange = function () {
        if (this.readyState === 4) {
            if (this.status === 200) {
                // Data.
                res = JSON.parse(this.responseText);
            
                // Update.
                if (res.length === 0)
                    moreAvailable = false;
                else {
                    productList = productList.concat(res);
                    viewList(res);
                }
            }
        }
    };
    xhttp.send(JSON.stringify({tags:tags, skip:productList.length}));
}

// Scroll actions.
scrollLeft.addEventListener('click', function() {
    scrollList.scrollTop = scrollList.scrollTop - 30;
});

scrollRight.addEventListener('click', function() {
    scrollList.scrollTop = scrollList.scrollTop + 30;
});

// Updater.
let changeActivePane = function(newActivePane) {
    activePane = newActivePane;

    for (var i = 0; i < searchBody.children.length; i++) {
        if (searchBody.children[i].id === activePane)
            searchBody.children[i].classList.remove('pane-hidden');
        else
            searchBody.children[i].classList.add('pane-hidden');
    }
}

let refreshList = function() {
    // Check if more available.
    if (moreAvailable)
        buttonMore.style.display = 'inline-block';
    else
        buttonMore.style.display = 'none';

    // Check if scroll is needed.
    if (scrollList.scrollHeight > scrollList.clientHeight) {
        scrollLeft.style.display = 'inline-block';
        scrollRight.style.display = 'inline-block';
    } else {
        scrollLeft.style.display = 'none';
        scrollRight.style.display = 'none';
    }
}

let viewProduct = function(product, id) {
    let img = product.img || 'assets/avatars/package.png';
    let cod = product.cod || 'Cod necunoscut';
    let titlu = product.titlu || 'Nume inexistent';
    let productRating = product.rating || '???';
    let infoRating = product.infoRating || '000';
    let price = product.price + ' RON' || '???';

    let isMinimized = active.getCurrActiveProduct() !== null ? 'style="display: none;"' : '';
    let isActive = active.getCurrActiveProduct() === id ? 'active' : '';

    let template = `
        <li id="searchlist-product-${id}" class="searchlist-product ${isActive}">
            <div class="uk-light uk-background-secondary uk-card">
                <div class="uk-light uk-card-header">
                    <div class="uk-grid-small uk-flex-middle" uk-grid>
                        <div class="cc-1 uk-width-auto" ${isMinimized}>
                            <img class="uk-border-circle" width="40" height="40" src="${img}">
                        </div>
                        <div class="uk-light uk-width-expand">
                            <h3 class="cc-2 uk-light uk-margin-remove-bottom">${titlu}</h3>
                            ${typeof cod !== 'undefined' && cod !== null
                              ? '<p class="uk-text-meta uk-margin-remove-top">'+cod+'</p>'
                              : ''}
                        </div>
                        <div class="cc-3 uk-width-auto" ${isMinimized}>
                            <i class="fas fa-star uk-align-center uk-margin-remove-bottom"></i>
                            <p class="uk-margin-remove-top">${productRating}</p>
                        </div>
                        <div class="cc-4 uk-width-auto" ${isMinimized}>
                            <i class="fas fa-dollar-sign uk-align-center uk-margin-remove-bottom"></i>
                            <p class="uk-margin-remove-top">${price}</p>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    `;

    return template;
}

let viewList = function(newProducts) {
    let keepIndex = scrollList.innerHTML.search(/<li\s+id="more"/);
    let rest = scrollList.innerHTML.substring(0, keepIndex);
    let keep = scrollList.innerHTML.substr(keepIndex);
    
    // Complete redraw.
    if (typeof newProducts === 'undefined') {
        if (productList !== null && productList.length > 0) {
            scrollList.innerHTML = productList.map(viewProduct).join('') + keep;
        }
    }
    // Append.
    else {
        //scrollList.innerHTML = rest + newProducts.map(viewProduct).join('') + keep;
        if (productList !== null && productList.length > 0) {
            scrollList.innerHTML = productList.map(viewProduct).join('') + keep;
        }
    }

    setTimeout(() => {
        // Bind all products.
        scrollList.querySelectorAll('.searchlist-product').forEach((item, idItem) => {
            item.addEventListener('click', () => {
                // Check if a switch or an open.
                let isSwitch = false;
                if (active.getCurrActiveProduct() !== null)
                    isSwitch = true;

                if (lockAnimation || active.getCurrActiveProduct() === idItem) return;

                // Change active element.
                active.changeActiveProduct(idItem, productList[idItem]);

                // Open detailed view.
                if (!isSwitch) {
                    lockAnimation = true;
                    panes.openPanes(() => lockAnimation = false);
                }
                else {
                    lockAnimation = true;
                    panes.switchPanes(() => lockAnimation = false);
                }
            });
        });

        // Rebind more button.
        buttonMore = document.getElementById('more');
        buttonMore.addEventListener('click', more);

        // Refresh list.
        refreshList();
    }, 50);
}

// Export stuff.
module.exports = {
    changeActivePane,
    refreshList,
    viewProduct,
    viewList,
    search,
    more,
    getActivePane: () => activePane,
    getProductList: () => productList
}
