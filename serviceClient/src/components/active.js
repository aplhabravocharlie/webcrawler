let scrollList = document.getElementById('searchlist');

let lastActiveProduct = null;
let currActiveProduct = null;
let product = null;

let changeActiveProduct = function(newActiveProduct, productData) {
    lastActiveProduct = currActiveProduct;
    currActiveProduct = newActiveProduct;
    product = productData;

    if (lastActiveProduct !== null) {
        scrollList.children[lastActiveProduct].classList.remove('active');
    }
    if (currActiveProduct !== null) {
        scrollList.children[currActiveProduct].classList.add('active');
    }
}

module.exports = {
    changeActiveProduct,
    getCurrActiveProduct: () => currActiveProduct,
    getCurrActiveProductData: () => product
}

