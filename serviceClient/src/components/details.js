let card = document.getElementById('card');

function renderProduct(product) {
    if (product === null) {
        card.innerHTML = '';
        return;
    }

    let img = product.img || 'assets/avatars/package.png';
    let cod = product.cod || 'Cod necunoscut';
    let titlu = product.titlu || 'Nume inexistent';
    let productRating = product.rating || '???';
    let infoRating = product.infoRating || '000';
    let descriere = product.descriere || 'Fără descriere.';
    let magazin = product.magazin;
    let price = product.price || '???';
    let url = product.url;

    // Prescurtare descriere.
    let limita = 200;
    if (descriere.length > 200)
        descriere = descriere.substring(0, 200) + '...';

    card.innerHTML = `
    <div class="uk-card details-card">
        <div class="uk-card-header uk-margin-remove-bottom">
            <div class="uk-flex-middle" uk-grid>
                <div class="uk-width-auto">
                    <img class="uk-border-circle" width="40" height="40" src="${img}">
                </div>
                <div class="uk-width-expand">
                    <h3 class="uk-card-title uk-margin-remove-bottom">${titlu}</h3>
                    <p class="uk-text-meta uk-margin-remove-top">${cod}</p>
                </div>
            </div>
        </div>
        <ul id="details-tab-master" 
            class="uk-child-width-expand details-tab uk-margin-remove-top" uk-tab>
            <li class="uk-active details-tab-generic"><a href="#">General</a></li>
            <li class="details-tab-generic"><a href="#">Recenzii</a></li>
            <li class="details-tab-generic"><a href="#">Surse</a></li>
            <li class="details-tab-generic"><a href="#">Preţ</a></li>
        </ul>
        <div id="details-tab-slave" class="uk-card-body">
            <div class="details-active details-tab-body">
                <article class="uk-article">
                    <h1 class="uk-article-title"><a class="uk-link-reset" href="">${titlu}</a></h1>                
                    <p class="uk-article-meta">Găsit în magazinul ${magazin}</p>
                    <p class="uk-text-lead">${descriere}</p>
                </article>
            </div>
            <div class="details-tab-body">
                ${product.comentarii.length === 0
                    ? "<p>Fără recenzii.</p>"
                    : `
                    <ul class="uk-list uk-list-divider">
                        ${product.comentarii.map(item => `<li>${item}</li>`)}
                    </ul>
                    `}
            </div>
            <div class="details-tab-body">
                <ul class="uk-list uk-list-divider">
                    <li>
                        <div class="uk-card uk-card-primary uk-card-body">
                            <h3 class="uk-card-title">${magazin}</h3>
                            <p><a class="details-no-drag" href="${url}">Apăsaţi aici pentru
                            a fi direcţionat către portalul ${magazin}</a></p>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="details-tab-body">
                <div id="chart"></div>
            </div>
        </div>
    </div>
    `;

    // Bind tab swapper.
    let detailsTabMaster = card.querySelector('#details-tab-master');
    let detailsTabSlave = card.querySelector('#details-tab-slave');
    card.querySelectorAll('.details-tab-generic').forEach((item, id) => {
        item.addEventListener('click', () => {
            // Links.
            for (var i = 0; i < detailsTabMaster.children.length; i++) {
                if (i !== id) detailsTabMaster.children[i].classList.remove('uk-active');
                else detailsTabMaster.children[i].classList.add('uk-active');
            }

            // Bodies.
            for (var i = 0; i < detailsTabSlave.children.length; i++) {
                if (i !== id) detailsTabSlave.children[i].classList.remove('details-active');
                else detailsTabSlave.children[i].classList.add('details-active');
            }

            // Chart.
            if (id === 3) {
                // Populate chart.
                let data = {
                    labels: [magazin],
            
                    datasets: [
                        {
                            title: "Preţuri",
                            values: [price]
                        },
                    ]
                };
            
                let chart = new Chart({
                    parent: "#chart",
                    title: "Comparare Preţuri",
                    data: data,
                    type: 'bar',
                    height: 250,
            
                    colors: ['#7cd6fd', 'violet', 'blue'],
            
                    format_tooltip_x: d => (d + '').toUpperCase(),
                    format_tooltip_y: d => d + ' pts'
                });
            }
        });
    });
}

module.exports = {
    renderProduct
}
