// Conectarea la canalul de mesaje.
let amqp = require('amqplib/callback_api');
let mq;

// Detalii/informatii configurare.
// Modifica aici daca schimbi host-ul canalului de mesaje.
let conf = {
    account: {
        domain: "https://www.cloudamqp.com/",
        mail: "w518062@mvrht.net",
        password: "1parolaaleatorie",
        instance: "proiectsecuritate2017"
    },

    serverUser: {
        username: 'rwwmjadp',
        password: 'fgYazOIp5UuWZJrA5OqvslANJQtUh4S5',
        server: 'impala.rmq.cloudamqp.com',
        instance: 'rwwmjadp',
    }

};

// Initializarea conexiunii la canalul de mesaje.
// Daca conectarea a esuat, chemam cbFail cu mesajul de eroare.
// Daca conectarea a reusit, chemam cbSuccess.
module.exports.init = function(cbSuccess, cbFail) {
    let connectionString = "amqp://";
    connectionString += conf.serverUser.username;
    connectionString += ":";
    connectionString += conf.serverUser.password;
    connectionString += "@";
    connectionString += conf.serverUser.server;
    connectionString += "/";
    connectionString += conf.serverUser.instance;

    amqp.connect(
        connectionString,
        function(err, amqpSocket) {
            if (err) {
                cbFail('Error: Could not connect to message broker.');
            } else {
                amqpSocket.createChannel(function(err, amqpClient) {
                    if (err) {
                        cbFail('Error: Could not create channel to message broker.');
                    } else {
                        mq = new RabbitMqEventEmitter(amqpClient);
                        cbSuccess();
                    }
                });
            }
        }
    );
}

// Dupa initializare, returneaza aceeasi conexiune.
module.exports.getClient = function() {
    return mq;
}

// Interfata compatibila cu EventEmitter nativ al NodeJS-ului.
class RabbitMqEventEmitter {
    constructor(mqClient) {
        this.mqClient = mqClient;
        this.assertedQueues = [];
    }

    on(event, callback) {
        if (this.assertedQueues.indexOf(event) === -1) {
            this.mqClient.assertQueue(event, { durable: false });
            this.assertedQueues.push(event);
        }

        this.mqClient.consume(event, function(data) {
            if (typeof data.content !== "undefined")
                callback(JSON.parse(data.content.toString()));
            else
                callback();
        }, { noAck: true });
    }

    emit(event, message) {
        if (this.assertedQueues.indexOf(event) === -1) {
            this.mqClient.assertQueue(event, { durable: false });
            this.assertedQueues.push(event);
        }

        if (typeof message !== "undefined")
            this.mqClient.sendToQueue(event, Buffer.from(JSON.stringify(message)));
        else
            this.mqClient.sendToQueue(event);
    }
}
