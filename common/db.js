// Conectarea la baza de date.
let mongodb = require('mongodb').MongoClient;
let db;

// Detalii/informatii configurare.
// Modifica aici daca schimbi host-ul bazei de date.
let conf = {
    account: {
        domain: "https://mlab.com",
        name: "proiectsecuritate2017",
        mail: "t369352@mvrht.net",
        username: "proiectsecuritate2017",
        password: "1parolaaleatorie"
    },

    databaseUser: {
        username: 'admin',
        password: '2o3mkdslm09ewmlked121m21w21l3dsmlk',
        database: 'webcrawler',
        server: 'ds161346.mlab.com',
        port: 61346
    }
};

// Initializarea conexiunii cu baza de date.
// Daca conectarea a esuat, chemam cbFail cu mesajul de eroare.
// Daca conectarea a reusit, chemam cbSuccess.
module.exports.init = function(cbSuccess, cbFail) {
    let connectionString = "mongodb://";
    connectionString += conf.databaseUser.username;
    connectionString += ":";
    connectionString += conf.databaseUser.password;
    connectionString += "@";
    connectionString += conf.databaseUser.server;
    connectionString += ":";
    connectionString += conf.databaseUser.port;
    connectionString += "/";
    connectionString += conf.databaseUser.database;

    mongodb.connect(
        connectionString,
        function(err, dbClient) {
            if (err) {
                cbFail('Error: Could not connect to database.');
            } else {
                db = dbClient.db(conf.databaseUser.database);
                cbSuccess();
            }
        }
    );
}

// Dupa initializare, returneaza aceeasi conexiune.
module.exports.getClient = function() {
    return db;
}
