let domain = 'https://proiectsecuritate2018.herokuapp.com';

let socket = require('socket.io-client')(domain, {
    transports: ['websocket'],
    upgrade: false
});

let ip = require('ip').address();

module.exports.getIp = function() {
    return ip;
}

module.exports.getSocket = function() {
    return socket;
}
