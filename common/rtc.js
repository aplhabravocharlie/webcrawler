// Componenta server WS.
let Server;
let ioServer;

// Initializarea modulului.
// Daca reuseste, cheama cbSuccess.
// Daca nu reuseste, cheama cbFail cu un mesaj de eroare.
module.exports.init = function(cbSuccess, cbFail) {
    Server = require('socket.io');
    cbSuccess();
}

// Returneaza serverul.
module.exports.getServer = function() {
    return ioServer;
}

// Seteaza serverul.
module.exports.setServer = function(http) {
    ioServer = new Server(http);
}

// Returneaza conexiunea pe port.
module.exports.getSocket = function() {
    return Server;
}
