// Componenta server HTTP.
let app;
let http;

// Initializarea modulului.
// Daca reuseste, cheama cbSuccess.
// Daca nu reuseste, cheama cbFail cu un mesaj de eroare.
module.exports.init = function(cbSuccess, cbFail) {
    app = require('express')();
    http = require('http').Server(app);
    cbSuccess();
}

// Returneaza serverul.
module.exports.getServer = function() {
    return app;
}

// Returneaza conexiunea pe port.
module.exports.getSocket = function() {
    return http;
}
