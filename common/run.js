// Rularea serviciului (ca server).
let path = require('path');
let exitHook = require('async-exit-hook');
let service = require(path.join(path.resolve('./'), process.argv[2]));

// Functii de rulare auxiliare.
function serviceStart(start) {
    let preHook = (resolve, reject) => resolve();
    let mainHook = (resolve, reject) => resolve();
    let postHook = (resolve, reject) => resolve();

    if (typeof service.init !== "undefined")
        mainHook = service.init;

    Promise.resolve()
    .then(() => new Promise(preHook))
    .then(() => new Promise(mainHook))
    .then(() => new Promise(postHook))
    .then(start)
    .catch(function(message) {
        // Unul dintre procese a esuat.
        console.log(message);
    });
}

function serviceEnd(end) {
    let preHook = (cb) => cb();
    let mainHook = (cb) => cb();
    let postHook = (cb) => cb();

    if (typeof service.final !== "undefined")
        mainHook = service.final;

    preHook(() => mainHook(() => postHook(end)));
}

// Sunt necesare cateva conexiuni.
// Conectarea este un proces asincron (nu stim cat timp ia).
Promise
.all(
    // Conexiunile necesare sunt declarate in serviciu.
    service.conf.dependencies.map(function(item) {
        return new Promise(require(item).init);
    })
)
.then(function() {
    // Toate conexiunile au pornit cu succes.

    // Initializam server HTTP.
    let http = null;
    if (service.conf.dependencies.filter(
        (item) => item.search(/http/) !== -1
    ).length > 0)
    {
        http = require('./http').getSocket();
        // Initializam RTC daca este necesar.
        if (service.conf.dependencies.filter(
            (item) => item.search(/rtc/) !== -1
        ).length > 0)
        {
            require('./rtc').setServer(http);
        }
    }

    // Pornim serviciul.
    serviceStart(function() {
        // Legam handler pentru iesirea serviciului.
        exitHook(serviceEnd);

        if (http !== null) {
            // Pornim serverul.
            http.listen(service.conf.port, function() {
                // Redam controlul inapoi serviciului.
                service.run();
            });
        }
        else {
            // Predam controlul serviciului.
            service.run();
        }
    });
})
.catch(function(message) {
    // Una dintre conexiuni a esuat.
    console.log(message);
});
